package pe.uni.mhuamanir.examenfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import com.bosphere.filelogger.FL;

public class MainActivity extends AppCompatActivity {

    Button buttonTrue,buttonFalse,buttonNext,buttonBack;
    ArrayList<Question> questions =new ArrayList<>();

    ArrayAdapter<String> questionsAdapter;
    ArrayList <String> questionsArray;
    String [] quest;

    ImageView imageView;
    TextView textViewQuest;

    int indexQuestion=0;
    boolean currentAnswer=false;
    Question currentQuestion;
    RelativeLayout relativeLayout;
    private static final String TAG=MainActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FL.i(TAG,R.string.file_start);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Resources r=getResources();
        quest=r.getStringArray(R.array.questions_strings);
        buttonBack=findViewById(R.id.back_button);
        buttonNext=findViewById(R.id.next_button);
        buttonTrue=findViewById(R.id.true_button);
        buttonFalse=findViewById(R.id.false_button);
         imageView=findViewById(R.id.image_quest);
         textViewQuest=findViewById(R.id.question);

        generateQuestions();
        updateQuest(indexQuestion);

        relativeLayout=findViewById(R.id.trivia);
        buttonTrue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentAnswer=true;
                if (currentQuestion.isAnswer()==currentAnswer) {
                    Snackbar.make(relativeLayout,R.string.correct,Snackbar.LENGTH_INDEFINITE).setAction("ok", v1 -> {}).show();
                }else{
                    Snackbar.make(relativeLayout,R.string.incorrect,Snackbar.LENGTH_INDEFINITE).setAction("ok", v1 -> {}).show();
                }
            }
        });
        buttonFalse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentAnswer=false;
                if (currentQuestion.isAnswer()==currentAnswer) {

                    Snackbar.make(relativeLayout,R.string.correct,Snackbar.LENGTH_INDEFINITE).setAction("ok", v1 -> {}).show();
                }else{
                    Snackbar.make(relativeLayout,R.string.incorrect,Snackbar.LENGTH_INDEFINITE).setAction("ok", v1 -> {}).show();
                }
            }
        });
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                indexQuestion+=1;
                if (indexQuestion>=6) {
                    indexQuestion=0;
                }
              updateQuest(indexQuestion);
            }
        });
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                indexQuestion-=1;
                if(indexQuestion<0){
                    indexQuestion=5;
                }
                updateQuest(indexQuestion);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        FL.i(TAG,R.string.file_end);
    }



    private  void generateQuestions(){
          questions.add(new Question(R.drawable.africa,quest[0],false));
          questions.add(new Question(R.drawable.espana,quest[1],true));
          questions.add(new Question(R.drawable.japon,quest[2],true));
          questions.add(new Question(R.drawable.panda,quest[3],false));
          questions.add(new Question(R.drawable.peru,quest[4],true));
          questions.add(new Question(R.drawable.sudamerica,quest[5],false));
    }
    private void updateQuest(int index){
        currentQuestion=questions.get(index);
       imageView.setImageResource(currentQuestion.getImage());
       textViewQuest.setText(currentQuestion.getQuestion_text());
    }
}