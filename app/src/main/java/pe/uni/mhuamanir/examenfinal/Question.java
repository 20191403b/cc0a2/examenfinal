package pe.uni.mhuamanir.examenfinal;

public class Question {
    Integer image=0;
    String question_text="";
    boolean answer=false;

    public Question(Integer image, String question_text, boolean answer) {
        this.image = image;
        this.question_text = question_text;
        this.answer = answer;
    }

    public Integer getImage() {
        return image;
    }

    public void setImage(Integer image) {
        this.image = image;
    }

    public String getQuestion_text() {
        return question_text;
    }

    public void setQuestion_text(String question_text) {
        this.question_text = question_text;
    }

    public boolean isAnswer() {
        return answer;
    }

    public void setAnswer(boolean answer) {
        this.answer = answer;
    }
}
